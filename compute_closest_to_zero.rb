def compute_closest_to_zero(ts)
  nums = ts.sort
  positive = nums.select { |number| number >= 0 }
  puts positive.first()
end

ts = [60, 70, 1, 80, -1]
compute_closest_to_zero(ts)